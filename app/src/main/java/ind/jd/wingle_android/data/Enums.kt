package ind.jd.wingle_android.data

enum class Status {
    NEW,
    IN_PROGRESS,
    COMPLETED,
    REJECTED,
    UNDEFINED
}

enum class TypeField {
    TEXT,
    SELECT,
    ATTACHMENT,
    UNDEFINED
}