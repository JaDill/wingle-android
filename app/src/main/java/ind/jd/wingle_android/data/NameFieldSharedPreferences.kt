package ind.jd.wingle_android.data

class NameFieldSharedPreferences private constructor() {

    fun token() = "TOKEN"

    fun fio() = "FIO"

    fun email() = "EMAIL"

    fun id() = "ID"

    fun title() = "TITLE"

    fun description() = "DESCRIPTION"

    fun firstStart() = "FIRST_START"

    fun defaultCourse() = "DEFAULT_COURSE"

    fun error() = "ERROR"

    fun filter() = "FILTER"

    companion object {
        private var mInstance: NameFieldSharedPreferences? = null

        val instance: NameFieldSharedPreferences
            get() {
                if (mInstance == null)
                    mInstance = NameFieldSharedPreferences()
                return mInstance as NameFieldSharedPreferences
            }
    }
}
