package ind.jd.wingle_android.data

import ind.jd.wingle_android.R

class Handler private constructor() {

    fun parseStringToRequestStatus(status: String) = when (status) {
        "new" -> Status.NEW
        "in_progress" -> Status.IN_PROGRESS
        "completed" -> Status.COMPLETED
        "rejected" -> Status.REJECTED
        else -> Status.UNDEFINED
    }

    fun parseFieldType(type: String) = when (type) {
        "text" -> TypeField.TEXT
        "select" -> TypeField.SELECT
        "attachment" -> TypeField.ATTACHMENT
        else -> TypeField.UNDEFINED
    }

    fun parseUserRoleFromServer(role: String?) = when (role) {
        "student" -> R.string.student
        else -> R.string.undefined
    }

    fun parseStringResourceToStringServerStatus(status: String): String = when (status) {
        "New", "Новая" -> "new"
        "In progress", "В работе" -> "in_progress"
        "Completed", "Обработана" -> "completed"
        "Rejected", "Отказано" -> "rejected"
        else -> ""
    }

    companion object {

        private var mInstance: Handler? = null

        val instance: Handler
            get() {
                if (mInstance == null)
                    mInstance = Handler()
                return mInstance as Handler
            }
    }
}
