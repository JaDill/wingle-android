package ind.jd.wingle_android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ind.jd.wingle_android.R
import ind.jd.wingle_android.server.responses.course.index.IndexRes

class CourseAdapter(private val context: Context, private val indexResList: List<IndexRes.Data>, private val mOnClickListener: (Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(context)
                .inflate(R.layout.cardview_course, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as ListViewHolder).bindView(i)
    }

    override fun getItemCount(): Int {
        return indexResList.size
    }

    private inner class ListViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val title: TextView = itemView.findViewById(R.id.COURSE_title)
        private var idCourse: Int = 0

        init {
            itemView.setOnClickListener(this)
        }

        internal fun bindView(position: Int) {
            idCourse = indexResList[position].id
            title.text = indexResList[position].title
        }

        override fun onClick(v: View) {
            mOnClickListener(idCourse)
        }
    }
}
