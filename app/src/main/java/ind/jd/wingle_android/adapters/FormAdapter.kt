package ind.jd.wingle_android.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ind.jd.wingle_android.R
import ind.jd.wingle_android.server.responses.form.index.IndexRes

class FormAdapter(private val mContext: Context, private val mDataList: List<IndexRes.Data>, private val mOnClickListener: (Int) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext)
                .inflate(R.layout.cardview_submission, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as ListViewHolder).bindView(i)
    }

    override fun getItemCount(): Int {
        return mDataList.size
    }

    private inner class ListViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val title: TextView = itemView.findViewById(R.id.SUBMISSION_title)
        private val description: TextView = itemView.findViewById(R.id.SUBMISSION_description)
        private var idForm: Int = 0

        init {
            itemView.setOnClickListener(this)
        }

        internal fun bindView(position: Int) {
            idForm = mDataList[position].id
            title.text = mDataList[position].title
            description.text = mDataList[position].description

        }

        override fun onClick(v: View) {
            mOnClickListener(idForm)
        }
    }
}
