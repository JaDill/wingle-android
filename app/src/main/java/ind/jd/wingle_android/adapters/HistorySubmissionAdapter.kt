package ind.jd.wingle_android.adapters

import android.content.Context
import android.content.Intent
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ind.jd.wingle_android.R
import ind.jd.wingle_android.activities.ReadSubmissionActivity
import ind.jd.wingle_android.data.Handler
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.data.Status
import ind.jd.wingle_android.server.responses.submission.readAll.ReadRes
import java.text.SimpleDateFormat
import java.util.*


class HistorySubmissionAdapter(private val mContext: Context, private var mListReadSubmissionsRes: List<ReadRes.Data>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    init {
        mListReadSubmissionsRes = mListReadSubmissionsRes.sortedByDescending { it.updatedAt }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view = LayoutInflater.from(mContext)
                .inflate(R.layout.cardview_history_submission, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, i: Int) {
        (viewHolder as ListViewHolder).bindView(i)
    }

    override fun getItemCount(): Int {
        return mListReadSubmissionsRes.size
    }

    private fun getDateWithoutTimeUsingFormat(date: Date): Date {
        val formatter = SimpleDateFormat("dd/MM/yyyy", Locale.getDefault())
        return formatter.parse(formatter.format(date))
    }

    private inner class ListViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {

        private val title: TextView = itemView.findViewById(R.id.HISTORY_SUBMISSION_title)
        private val description: TextView = itemView.findViewById(R.id.HISTORY_SUBMISSION_description)
        private val status: CardView = itemView.findViewById(R.id.HISTORY_SUBMISSION_status_view)
        private val statusText: TextView = itemView.findViewById(R.id.HISTORY_SUBMISSION_status_text)
        private val requestDate: TextView = itemView.findViewById(R.id.HISTORY_SUBMISSION_request_date)
        private var idSubmission: Int = 0

        init {
            itemView.setOnClickListener(this)
        }

        internal fun bindView(position: Int) {
            idSubmission = mListReadSubmissionsRes[position].id!!
            title.text = mListReadSubmissionsRes[position].form!!.title
            if (mListReadSubmissionsRes[position].form!!.description == null || mListReadSubmissionsRes[position].form!!.description === "")
                description.visibility = View.GONE
            else {
                description.visibility = View.VISIBLE
                description.text = mListReadSubmissionsRes[position].form!!.description
            }
            when (Handler.instance.parseStringToRequestStatus(mListReadSubmissionsRes[position].status!!)) {
                Status.NEW -> {
                    statusText.setText(R.string.status_request_new)
                    status.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.request_new))
                }
                Status.COMPLETED -> {
                    statusText.setText(R.string.status_request_completed)
                    status.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.request_ready))
                }
                Status.REJECTED -> {
                    statusText.setText(R.string.status_request_rejected)
                    status.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.request_error))
                }
                Status.IN_PROGRESS -> {
                    statusText.setText(R.string.status_request_in_progress)
                    status.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.request_warning))
                }
                else -> {
                    statusText.setText(R.string.undefined)
                    status.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.request_error))
                }
            }
            val now = getDateWithoutTimeUsingFormat(Date())
            val updatedAt = mListReadSubmissionsRes[position].updatedAt

            val format = if (now == getDateWithoutTimeUsingFormat(updatedAt))
                SimpleDateFormat("HH:mm", Locale.getDefault())
            else
                SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            requestDate.text = format.format(updatedAt)
        }


        override fun onClick(v: View) {
            val intent = Intent(mContext, ReadSubmissionActivity::class.java)
            intent.putExtra(NameFieldSharedPreferences.instance.id(), idSubmission)
            intent.putExtra(NameFieldSharedPreferences.instance.title(), title.text)
            intent.putExtra(NameFieldSharedPreferences.instance.description(), description.text)
            mContext.startActivity(intent)
        }
    }
}
