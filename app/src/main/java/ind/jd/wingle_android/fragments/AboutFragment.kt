package ind.jd.wingle_android.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import ind.jd.wingle_android.R

class AboutFragment : Fragment() {
    private var viewFragment: View? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        viewFragment = inflater.inflate(R.layout.fragment_about, container, false)
        val listAbout = resources.getStringArray(R.array.about)
        var textAbout = ""
        for (i in 0 until listAbout.size)
            textAbout = textAbout + "\n" + listAbout[i]
        val aboutTextView: TextView = viewFragment!!.findViewById(R.id.ABOUT_text)
        aboutTextView.text = textAbout
        return viewFragment
    }
}
