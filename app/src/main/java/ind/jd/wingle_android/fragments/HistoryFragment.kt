package ind.jd.wingle_android.fragments


import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.activities.LoginActivity
import ind.jd.wingle_android.activities.MainActivity
import ind.jd.wingle_android.adapters.HistorySubmissionAdapter
import ind.jd.wingle_android.data.Handler
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.submission.readAll.ReadRes
import retrofit2.Call
import retrofit2.Response
import java.net.UnknownHostException

class HistoryFragment : Fragment(), SwipeRefreshLayout.OnRefreshListener {
    private var mContext: Context? = null
    private var mViewFragment: View? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var mHistoryView: RecyclerView? = null
    private var mNoHistoryView: LinearLayout? = null
    private var mProgressView: LinearLayout? = null
    private var mSh: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mViewFragment = inflater.inflate(R.layout.fragment_history, container, false)

        initializeFields()

        return mViewFragment
    }

    override fun onResume() {
        super.onResume()
        onRefresh()
    }

    private fun initializeFields() {
        mContext = mViewFragment!!.context
        mSh = mContext!!.getSharedPreferences("Data", MODE_PRIVATE)
        mSwipeRefreshLayout = mViewFragment!!.findViewById(R.id.HISTORY_refresh_view)
        mSwipeRefreshLayout!!.setOnRefreshListener(this)
        mHistoryView = mViewFragment!!.findViewById(R.id.HISTORY_history_recycler)
        val layoutManager = LinearLayoutManager(mContext)
        mHistoryView!!.layoutManager = layoutManager
        mNoHistoryView = mViewFragment!!.findViewById(R.id.HISTORY_history_no_elements)
        mProgressView = mViewFragment!!.findViewById(R.id.HISTORY_load_history)

    }

    private fun showProgress(isVisible: Boolean) {
        mProgressView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            mHistoryView!!.visibility = View.GONE
            mNoHistoryView!!.visibility = View.GONE
        }
    }

    private fun showHistory(isFullHistory: Boolean) {
        mHistoryView!!.visibility = if (isFullHistory) View.VISIBLE else View.GONE
        mNoHistoryView!!.visibility = if (isFullHistory) View.GONE else View.VISIBLE
        mSwipeRefreshLayout!!.isRefreshing = false
    }

    override fun onRefresh() {
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance.jsonApi
                .getMySubmissions(token, (activity as MainActivity).mListFilterStatus!!.map { Handler.instance.parseStringResourceToStringServerStatus(it) })
                .enqueue(object : retrofit2.Callback<ReadRes> {
                    override fun onResponse(call: Call<ReadRes>, response: Response<ReadRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200) {
                            if (response.body()!!.data!!.isNotEmpty()) {
                                val listAdapter = HistorySubmissionAdapter(mContext!!, response.body()!!.data!!)
                                mHistoryView!!.adapter = listAdapter
                                showHistory(true)
                            } else {
                                showHistory(false)
                            }
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetHistoryTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ReadRes>, t: Throwable) {
                        showProgress(false)
                        showHistory(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetHistoryTask\n\n" + t.message)
                    }
                })
    }

    private fun startLoginActivity() {
        startActivity(Intent(this.context, LoginActivity::class.java))
        activity!!.finish()
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mViewFragment!!, message, Snackbar.LENGTH_LONG).show()
    }
}

