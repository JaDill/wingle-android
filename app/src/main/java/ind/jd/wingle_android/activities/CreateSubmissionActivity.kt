package ind.jd.wingle_android.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.util.Pair
import android.util.SparseArray
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.stackoverflow.PathUtil
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.data.Handler
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.data.TypeField
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.attachment.create.CreateRes
import ind.jd.wingle_android.server.responses.form.read.ReadRes
import ind.jd.wingle_android.server.responses.submission.create.CreateReq
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.UnknownHostException
import java.util.*
import java.util.regex.Pattern

class CreateSubmissionActivity : AppCompatActivity(), View.OnClickListener {

    private var _statusReadExternalStorage = 0

    private var mIdForm: Int = 0
    private var mMainContainer: View? = null
    private var mToolbar: Toolbar? = null
    private var mTitle: TextView? = null
    private var mDescription: TextView? = null
    private var mFieldsLayout: LinearLayout? = null
    private var mFieldsLinks: SparseArray<Pair<Boolean, *>>? = null
    private var mProgress: ProgressBar? = null
    private var mSubmitButton: CardView? = null
    private var mFileFields: MutableList<Int>? = null
    private var mMapToSubmit: SparseArray<Any>? = null
    private var mSh: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_submission)

        initializeFields()
        getFormAsync()
    }

    private fun initializeFields() {
        mFileFields = ArrayList()
        mMapToSubmit = SparseArray()
        mFieldsLinks = SparseArray()

        mMainContainer = findViewById(R.id.CREATE_SUBMISSION)
        mSh = getSharedPreferences("Data", MODE_PRIVATE)

        val intent = intent
        mIdForm = intent.getIntExtra(NameFieldSharedPreferences.instance.id(), 0)

        mFieldsLayout = findViewById(R.id.CREATE_SUBMISSION_fields)
        mProgress = findViewById(R.id.CREATE_SUBMISSION_progress)
        mSubmitButton = findViewById(R.id.CREATE_SUBMISSION_submit_button)
        mSubmitButton!!.setOnClickListener(this)

        mToolbar = findViewById(R.id.CREATE_SUBMISSION_toolbar)
        mToolbar!!.title = intent.getStringExtra(NameFieldSharedPreferences.instance.title())
        setSupportActionBar(mToolbar)
        try {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        } catch (ex: NullPointerException) {
            ex.printStackTrace()
        }

        mTitle = findViewById(R.id.CREATE_SUBMISSION_title)
        mTitle!!.text = intent.getStringExtra(NameFieldSharedPreferences.instance.title())
        mDescription = findViewById(R.id.CREATE_SUBMISSION_description)
        mDescription!!.text = intent.getStringExtra(NameFieldSharedPreferences.instance.description())
    }

    private fun getFormAsync() {
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance.jsonApi
                .getFormWithID(token, mIdForm)
                .enqueue(object : Callback<ReadRes> {
                    override fun onResponse(call: Call<ReadRes>, response: Response<ReadRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            updateFields(response.body()!!)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetFormTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ReadRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetFormTask\n\n" + t.message)
                    }
                })
    }

    private fun checkReadExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE))
                Snackbar.make(mMainContainer!!, R.string.explanation_for_request_ex_storage_permission, Snackbar.LENGTH_LONG)
                        .setAction(R.string.grant) { requestReadExtStorage() }.show()
            else
                showNoStoragePermissionSnackBar()
        } else
            _statusReadExternalStorage = 1
    }

    private fun requestReadExtStorage() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), _statusReadExternalStorage)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun updateFields(body: ReadRes) {
        val fields = body.data!!.fields
        mToolbar!!.title = body.data.title
        mTitle!!.text = body.data.title
        mDescription!!.text = body.data.description

        mFieldsLinks!!.clear()
        mFileFields!!.clear()

        for (i in fields!!.indices)
            addField(fields[i])

        showFields(true)
    }

    private fun addField(field: ReadRes.Data.Field) {
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, resources.displayMetrics).toInt()
        val oneFieldLinearLayout = LinearLayout(this)
        oneFieldLinearLayout.orientation = LinearLayout.VERTICAL

        addFieldTitle(oneFieldLinearLayout, field, px)

        addFieldDescription(oneFieldLinearLayout, field, px)

        when (Handler.instance.parseFieldType(field.type!!)) {
            TypeField.TEXT -> {
                addTextField(oneFieldLinearLayout, field, px)
            }
            TypeField.SELECT -> {
                if (field.meta!!.multiple!!)
                    addCheckboxField(oneFieldLinearLayout, field, px)
                else
                    addRadioGroupField(oneFieldLinearLayout, field, px)
            }
            TypeField.ATTACHMENT -> {
                addAttachmentField(oneFieldLinearLayout, field)
            }
            else -> {
            }
        }
        val oneFieldLinearLayoutll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
        mFieldsLayout!!.addView(oneFieldLinearLayout, oneFieldLinearLayoutll)
    }

    private fun addFieldTitle(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field, px: Int) {
        val textViewTitle = TextView(this)
        textViewTitle.textSize = 20f
        textViewTitle.typeface = Typeface.DEFAULT_BOLD
        var title = field.title!!
        textViewTitle.text = title
        // one way to add * in the end
        // if android version >= 7.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && field.required!!) {
            title += "<font color=#ee3333>*</font>"
            textViewTitle.text = Html.fromHtml(title, 1)
        }
        // if android version is less than 7.0
        else if (field.required!!) {
            title += "*"
            textViewTitle.text = title
        }
        val titlell = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        titlell.setMargins(0, 0, 0, 8 * px)
        oneFieldLinearLayout.addView(textViewTitle, titlell)
    }

    private fun addFieldDescription(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field, px: Int) {
        if (field.description != null && field.description != "") {
            val textViewDescription = TextView(this)
            textViewDescription.text = field.description
            textViewDescription.textSize = 14f
            val descriptionll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            descriptionll.setMargins(8 * px, 0, 0, 4 * px)
            oneFieldLinearLayout.addView(textViewDescription, descriptionll)
        }
    }

    private fun addTextField(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field, px: Int) {
        val editTextValue = EditText(this)
        val valuell = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        valuell.setMargins(0, 0, 0, 12 * px)
        oneFieldLinearLayout.addView(editTextValue, valuell)

        mFieldsLinks!!.put(field.id!!, Pair<Boolean, EditText>(field.required, editTextValue))
    }

    private fun addCheckboxField(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field, px: Int) {
        val linearLayoutGroup = LinearLayout(this)
        linearLayoutGroup.orientation = LinearLayout.VERTICAL

        val groupll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        groupll.setMargins(0, 0, 0, 12 * px)

        val checkboxll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        checkboxll.setMargins(0, 0, 0, 4 * px)

        val localMap = HashMap<String, CheckBox>()

        for (i in 0 until field.meta!!.options!!.size) {
            val checkbox = CheckBox(linearLayoutGroup.context)
            val value = field.meta.options!![i]
            checkbox.text = value
            linearLayoutGroup.addView(checkbox, checkboxll)

            localMap[field.meta.options[i]] = checkbox
        }
        oneFieldLinearLayout.addView(linearLayoutGroup, groupll)

        mFieldsLinks!!.put(field.id!!, Pair<Boolean, Map<String, CheckBox>>(field.required, localMap))
    }

    private fun addRadioGroupField(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field, px: Int) {
        val radioGroup = RadioGroup(this)
        radioGroup.orientation = LinearLayout.VERTICAL
        val radioGroupll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioGroupll.setMargins(0, 0, 0, 12 * px)

        val radioButtonll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButtonll.setMargins(0, 0, 0, 4 * px)

        val localMap = HashMap<String, RadioButton>()

        for (i in 0 until field.meta!!.options!!.size) {
            val radioButton = RadioButton(radioGroup.context)
            val value = field.meta.options!![i]
            radioButton.text = value
            radioGroup.addView(radioButton, radioButtonll)

            localMap[field.meta.options[i]] = radioButton
        }
        oneFieldLinearLayout.addView(radioGroup, radioGroupll)

        mFieldsLinks!!.put(field.id!!, Pair<Boolean, Map<String, RadioButton>>(field.required, localMap))
    }

    private fun addAttachmentField(oneFieldLinearLayout: LinearLayout, field: ReadRes.Data.Field) {
        val download = LinearLayout(this)
        download.orientation = LinearLayout.HORIZONTAL
        val button = Button(this)
        button.setText(R.string.choose_file)
        download.addView(button)

        val realUri = TextView(this)
        realUri.visibility = View.GONE
        download.addView(realUri)

        val pathForUser = TextView(this)
        pathForUser.setText(R.string.path_to_file)
        download.addView(pathForUser)

        val id = mFileFields!!.size
        mFileFields!!.add(field.id!!)

        val downloadll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        button.setOnClickListener {
            checkReadExternalStoragePermission()
            if (_statusReadExternalStorage == 1)
                showFileChooser(id)
        }
        oneFieldLinearLayout.addView(download, downloadll)
        mFieldsLinks!!.put(field.id, Pair<Boolean, Pair<TextView, TextView>>(field.required, Pair(realUri, pathForUser)))
    }

    private fun showNoStoragePermissionSnackBar() {
        Snackbar.make(mMainContainer!!, R.string.no_ex_storage_permission, Snackbar.LENGTH_LONG)
                .setAction(R.string.settings) {
                    openApplicationSettings()

                    Toast.makeText(applicationContext,
                            R.string.prompt_how_to_grant_ex_storage_permission,
                            Toast.LENGTH_SHORT)
                            .show()
                }
                .show()
    }

    private fun openApplicationSettings() {
        val appSettingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:$packageName"))
        startActivityForResult(appSettingsIntent, -1)
    }

    private fun showProgress(isVisible: Boolean) {
        mProgress!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            mFieldsLayout!!.visibility = View.GONE
            mSubmitButton!!.visibility = View.GONE
        }
    }

    private fun showFields(isVisible: Boolean) {
        mFieldsLayout!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        mSubmitButton!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun showFileChooser(idField: Int) {
        val intent = Intent()
        intent.type = "*/*"
        val mimeTypes = arrayOf("image/jpeg", "image/pjpeg", "image/png", "application/pdf", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Choose file to Upload.."), idField)
    }

    private fun checkSelectedFile(uri: Uri): Boolean {
        val pathToFile = PathUtil.getPath(this@CreateSubmissionActivity, uri)
        if (pathToFile == null || pathToFile == "") {
            showMessageSnackBar(getString(R.string.file_not_found))
            return false
        }
        val file = File(pathToFile)
        if (file.length() >= 5 * 1024 * 1024) {
            showMessageSnackBar(getString(R.string.limit_size))
            return false
        }
        val pattern = Pattern.compile("\\.(jpg|jpeg|png|doc|docx|xls|xlsx|pdf)$")
        if (!pattern.matcher(file.name).find()) {
            showMessageSnackBar(getString(R.string.allow_file_format))
            return false
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // user chose file to upload
        if (resultCode == Activity.RESULT_OK) {
            if (data == null)
                return

            val selectedFileUri = data.data!!
            if (checkSelectedFile(selectedFileUri)) {
                val pathToFile = PathUtil.getPath(this@CreateSubmissionActivity, selectedFileUri)
                val file = File(pathToFile)
                val path = mFieldsLinks!!.get(mFileFields!![requestCode]).second as Pair<*, *>
                (path.first as TextView).text = selectedFileUri.toString()
                (path.second as TextView).text = file.name
            }
        }
        // request ex storage permission
        else if (requestCode == -1) {
            _statusReadExternalStorage = 1
        }
    }

    override fun onClick(v: View) {
        mMapToSubmit!!.clear()
        val mapToDownloadFile = SparseArray<Pair<String, String>>()
        var oneRequiredFieldIsEmpty = false

        for (i in 0 until mFieldsLinks!!.size()) {
            val key = mFieldsLinks!!.keyAt(i)
            // field is EditText
            // just get text
            if (mFieldsLinks!!.get(key).second is EditText) {
                val text = (mFieldsLinks!!.get(key).second as EditText).text.toString()
                if (text.isEmpty() && mFieldsLinks!!.get(key).first) {
                    oneRequiredFieldIsEmpty = true
                    break
                }
                if (text.isNotEmpty())
                    mMapToSubmit!!.put(key, text)
            }
            // field is multiply choice (checkbox, radio)
            // check all little fields and get checked only
            else if (mFieldsLinks!!.get(key).second is Map<*, *>) {
                var checkedRadiobutton: String? = null
                val checkedCheckbox = ArrayList<String>()

                val ref = mFieldsLinks!!.get(key).second as Map<*, *>
                for (key2 in ref.keys) {
                    if (ref[key2] is RadioButton) {
                        val ch = (ref[key2] as RadioButton).isChecked
                        if (ch) {
                            checkedRadiobutton = key2 as String
                            break
                        }
                    } else if (ref[key2] is CheckBox) {
                        val ch = (ref[key2] as CheckBox).isChecked
                        if (ch)
                            checkedCheckbox.add(key2 as String)
                    }
                }

                if (checkedCheckbox.isEmpty() && checkedRadiobutton == null && mFieldsLinks!!.get(key).first) {
                    oneRequiredFieldIsEmpty = true
                    break
                }
                if (checkedCheckbox.isNotEmpty() || checkedRadiobutton != null)
                    mMapToSubmit!!.put(key, if (checkedCheckbox.isEmpty()) checkedRadiobutton else checkedCheckbox)
            }
            // field is attachment
            // get file from uri and prepare to upload
            else if (mFieldsLinks!!.get(key).second is Pair<*, *>) {
                val uri = ((mFieldsLinks!!.get(key).second as Pair<*, *>).first as TextView).text.toString()
                val filename = ((mFieldsLinks!!.get(key).second as Pair<*, *>).second as TextView).text.toString()
                if (uri.isEmpty() && mFieldsLinks!!.get(key).first) {
                    oneRequiredFieldIsEmpty = true
                    break
                }
                if (uri.isNotEmpty())
                    mapToDownloadFile.put(key, Pair(uri, filename))
            }
        }
        // check all required fields are filled
        if (oneRequiredFieldIsEmpty) {
            showMessageSnackBar(getString(R.string.error_empty_field))
            return
        }

        // run server request
        showProgress(true)
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        if (mapToDownloadFile.size() != 0)
            uploadFilesAsync(token, mapToDownloadFile)
        else
            sendSubmissionAsync(token)
    }

    private fun uploadFilesAsync(token: String, mapToDownloadFile: SparseArray<Pair<String, String>>) {
        try {
            var sizeOfSuccessUpload = 0
            var criticalSection = true
            for (i in 0 until mapToDownloadFile.size()) {
                val key = mapToDownloadFile.keyAt(i)
                val value = mapToDownloadFile.get(key)
                val fileUri = value.first
                val name = value.second
                val path = PathUtil.getPath(this@CreateSubmissionActivity, Uri.parse(fileUri))
                val file = File(path)
                val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
                val body = MultipartBody.Part.createFormData("file", file.name, requestFile)
                val rName = RequestBody.create(MediaType.parse("multipart/form-data"), name)

                NetworkService.instance.jsonApi
                        .uploadFile(token, rName, body)
                        .enqueue(object : Callback<CreateRes> {
                            override fun onResponse(call: Call<CreateRes>, response: Response<CreateRes>) {
                                // unauthorized
                                if (response.code() == 401) {
                                    startLoginActivity()
                                    throw IllegalStateException()
                                }
                                // success
                                else if (response.code() == 200 && response.body() != null) {
                                    mMapToSubmit!!.put(key, response.body()!!.data!!.id)
                                    sizeOfSuccessUpload++
                                    if (BuildConfig.DEBUG)
                                        Log.i("JaDill", "OK in UploadFileTask")

                                    // kostil!
                                    if (sizeOfSuccessUpload == mapToDownloadFile.size() && criticalSection) {
                                        criticalSection = false
                                        sendSubmissionAsync(token)
                                    }
                                } // unexpected error
                                else {
                                    showMessageSnackBar(getString(R.string.error_server))
                                    throw IllegalStateException()
                                }
                            }

                            override fun onFailure(call: Call<CreateRes>, t: Throwable) {
                                showMessageSnackBar(getString(
                                        if (t is UnknownHostException)
                                            R.string.no_internet
                                        else
                                            R.string.error_file_upload)
                                )

                                if (BuildConfig.DEBUG)
                                    Log.e("JaDill", "Throw exception in UploadFileTask\n\n" + t.message)
                                throw IllegalStateException()
                            }
                        })
            }
        } catch (ex: IllegalStateException) {
            showProgress(false)
            showFields(true)
        }
    }

    @Synchronized
    private fun sendSubmissionAsync(token: String) {
        val createReq = CreateReq()
        run {
            val answers = ArrayList<CreateReq.Answer>()
            for (i in 0 until mMapToSubmit!!.size()) {
                val key = mMapToSubmit!!.keyAt(i)
                val answer = createReq.Answer()
                answer.fieldId = key
                answer.value = mMapToSubmit!!.get(key)
                answers.add(answer)
            }
            createReq.answers = answers

            createReq.formId = mIdForm
        }

        NetworkService.instance.jsonApi
                .createSubmission(token, createReq)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200) {
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in SendFormTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in UploadFileTask\n\n" + t.message)
                    }
                })
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainContainer!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
