package ind.jd.wingle_android.activities

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment

import com.github.paolorotolo.appintro.AppIntro

import ind.jd.wingle_android.R
import ind.jd.wingle_android.fragments.SampleSlide

class IntroActivity : AppIntro() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addSlide(SampleSlide.newInstance(R.layout.intro_1))
        addSlide(SampleSlide.newInstance(R.layout.intro_2))
        addSlide(SampleSlide.newInstance(R.layout.intro_3))
        addSlide(SampleSlide.newInstance(R.layout.intro_4))
        addSlide(SampleSlide.newInstance(R.layout.intro_5))
    }

    private fun closeOnBoarding() {
        startActivity(Intent(this@IntroActivity, MainActivity::class.java))
        finish()
    }

    override fun onSkipPressed(currentFragment: Fragment?) {
        super.onSkipPressed(currentFragment)
        closeOnBoarding()
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        closeOnBoarding()
    }
}
