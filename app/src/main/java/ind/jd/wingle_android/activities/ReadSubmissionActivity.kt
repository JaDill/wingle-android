package ind.jd.wingle_android.activities

import android.Manifest
import android.app.DownloadManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.Settings
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.text.Html
import android.util.Log
import android.util.TypedValue
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.stackoverflow.PathUtil
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.data.Handler
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.data.Status
import ind.jd.wingle_android.data.TypeField
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.attachment.create.CreateRes
import ind.jd.wingle_android.server.responses.attachment.get.GetRes
import ind.jd.wingle_android.server.responses.submission.patchWithId.PatchReq
import ind.jd.wingle_android.server.responses.submission.readWithId.ReadRes
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.UnknownHostException
import java.util.*
import java.util.regex.Pattern

class ReadSubmissionActivity : AppCompatActivity(), SwipeRefreshLayout.OnRefreshListener {
    private var _statusReadExternalStorage = 0
    private var _statusWriteExternalStorage = 0

    private var mIdSubmission: Int = 0
    private var mSh: SharedPreferences? = null
    private var mStatus: Status? = null

    private var mMainView: View? = null
    private var mToolbarView: Toolbar? = null
    private var mTitleView: TextView? = null
    private var mDescriptionView: TextView? = null
    private var mStatusView: CardView? = null
    private var mStatusTextView: TextView? = null
    private var mFieldsLayout: LinearLayout? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var mProgressView: ProgressBar? = null

    // for check only 1 field is edited now
    private var mEditActiveButton: View? = null

    private var mIsViewGroupRequired: MutableMap<List<View>, Boolean>? = null
    private var mListViewGroupToFieldId: MutableMap<List<View>, Int>? = null
    private var mListViewGroupToAttachmentId: MutableMap<List<View>, Int>? = null

    private var mmIdAttachment: Int = 0
    private var mmIdField: Int = 0
    private var mmPathForUser: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_read_submission)

        initializeFields()
        checkReadExternalStoragePermission()
        checkWriteExternalStoragePermission()
        getSubmissionAsync()
    }

    private fun initializeFields() {
        mIsViewGroupRequired = HashMap()
        mListViewGroupToFieldId = HashMap()
        mListViewGroupToAttachmentId = HashMap()
        mSh = getSharedPreferences("Data", MODE_PRIVATE)

        mMainView = findViewById(R.id.READ_SUBMISSION)

        val intent = intent
        mIdSubmission = intent.getIntExtra(NameFieldSharedPreferences.instance.id(), 0)

        mFieldsLayout = findViewById(R.id.READ_SUBMISSION_fields)
        mProgressView = findViewById(R.id.READ_SUBMISSION_progress)
        mSwipeRefreshLayout = findViewById(R.id.READ_SUBMISSION_refresh_view)
        mSwipeRefreshLayout!!.setOnRefreshListener(this)

        mToolbarView = findViewById(R.id.READ_SUBMISSION_toolbar)
        mToolbarView!!.title = intent.getStringExtra(NameFieldSharedPreferences.instance.title())
        setSupportActionBar(mToolbarView)
        try {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        mTitleView = findViewById(R.id.READ_SUBMISSION_title)
        mTitleView!!.text = intent.getStringExtra(NameFieldSharedPreferences.instance.title())
        mDescriptionView = findViewById(R.id.READ_SUBMISSION_description)
        mDescriptionView!!.text = intent.getStringExtra(NameFieldSharedPreferences.instance.description())
        mStatusView = findViewById(R.id.READ_SUBMISSION_status_view)
        mStatusTextView = findViewById(R.id.READ_SUBMISSION_status_text)
        setStatus(Status.UNDEFINED)
    }

    private fun getSubmissionAsync() {
        showProgress(true)
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!

        NetworkService.instance.jsonApi
                .getMySubmissionWithID(token, mIdSubmission)
                .enqueue(object : Callback<ReadRes> {
                    override fun onResponse(call: Call<ReadRes>, response: Response<ReadRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            updateFields(response.body()!!)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetSubmissionTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ReadRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetSubmissionTask\n\n" + t.message)
                    }
                })
    }

    private fun editFieldAsync(idField: Int, userAnswers: Any, needRefreshAfter: Boolean) {
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")
        ////////////////////////////////////////////////////////////////////////////////////////
        val patchReq = PatchReq()
        run {
            val answer = patchReq.Answer()
            val answers = ArrayList<PatchReq.Answer>()
            answers.add(answer)

            answer.fieldId = idField
            answer.value = userAnswers

            patchReq.answers = answers
        }
        ////////////////////////////////////////////////////////////////////////////////////////
        NetworkService.instance.jsonApi
                .editFieldWithID(token!!, mIdSubmission, patchReq)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        showProgress(false)
                        showFieldsLayout(true)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200) {
                            showMessageSnackBar(getString(R.string.changes_saved))
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in EditFieldTask")
                            if (needRefreshAfter)
                                onRefresh()
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showProgress(false)
                        showFieldsLayout(true)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in EditFieldTask\n\n" + t.message)
                    }
                })
    }

    private fun deleteSubmissionAsync() {
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!

        NetworkService.instance.jsonApi
                .deleteSubmission(token, mIdSubmission)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 204) {
                            finish()
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in DeleteSubmissionTask")
                        } // unexpected error
                        else {
                            showProgress(false)
                            showFieldsLayout(true)
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showFieldsLayout(true)
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_file_upload)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in DeleteSubmissionTask\n\n" + t.message)
                    }
                })
    }

    private fun uploadFileAsync(uri: Uri) {
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        val path = PathUtil.getPath(this@ReadSubmissionActivity, uri)
        val file = File(path)
        val name = file.name
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("file", name, requestFile)
        val rName = RequestBody.create(MediaType.parse("multipart/form-data"), name)

        NetworkService.instance.jsonApi
                .uploadFile(token, rName, body)
                .enqueue(object : Callback<CreateRes> {
                    override fun onResponse(call: Call<CreateRes>, response: Response<CreateRes>) {
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            editFieldAsync(mmIdField, response.body()!!.data!!.id!!, true)
                            showMessageSnackBar(getString(R.string.all_files_upload))
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in UploadFileTask")
                        } // unexpected error
                        else {
                            showProgress(false)
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<CreateRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_file_upload)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in UploadFileTask\n\n" + t.message)
                    }
                })
    }

    private fun editFileAsync(uri: Uri) {
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        val path = PathUtil.getPath(this@ReadSubmissionActivity, uri)
        val file = File(path)
        val name = file.name
        val requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val body = MultipartBody.Part.createFormData("file", name, requestFile)
        val rName = RequestBody.create(MediaType.parse("multipart/form-data"), name)
        val rMethod = RequestBody.create(MediaType.parse("multipart/form-data"), "patch")

        NetworkService.instance.jsonApi
                .editFile(token, mmIdAttachment, rName, body, rMethod)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        showProgress(false)
                        showFieldsLayout(true)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200) {
                            setNewPathForUser(name)
                            showMessageSnackBar(getString(R.string.all_files_upload))
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in EditFileTask")
                        } // unexpected error
                        else {
                            showProgress(false)
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showFieldsLayout(true)
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_file_upload)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in EditFileTask\n\n" + t.message)
                    }
                })
    }

    private fun deleteFileAsync(idAttachment: Int) {
        showProgress(true)
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!

        NetworkService.instance.jsonApi
                .deleteAttachment(token, idAttachment)
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        showProgress(false)
                        showFieldsLayout(true)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 204) {
                            onRefresh()
                            showMessageSnackBar(getString(R.string.file_deleted))
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in DeleteFileTask")
                        } // unexpected error
                        else {
                            showProgress(false)
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showFieldsLayout(true)
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_file_upload)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in DeleteFileTask\n\n" + t.message)
                    }
                })
    }

    private fun downloadFileAsync(idAttachment: Int) {
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!

        NetworkService.instance.jsonApi
                .getAttachmentInfo(token, idAttachment)
                .enqueue(object : Callback<GetRes> {
                    override fun onResponse(call: Call<GetRes>, response: Response<GetRes>) {
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {

                            val uri = Uri.parse(NetworkService.instance.baseUrl + "attachment/" + idAttachment + "/download")
                            val request = DownloadManager.Request(uri)
                                    .addRequestHeader("Authorization", token)
                                    .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                                    .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, response.body()!!.data!!.name)
                            val dm = getSystemService(DOWNLOAD_SERVICE) as DownloadManager
                            dm.enqueue(request)

                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in DownloadFileTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<GetRes>, t: Throwable) {
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in DownloadFileTask\n\n" + t.message)
                    }
                })
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.read_submission_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.READ_SUBMISSION_MENU_delete)
            deleteSubmissionAsync()
        else
            Snackbar.make(mMainView!!, R.string.action_is_not_exist, Snackbar.LENGTH_LONG).show()
        return super.onOptionsItemSelected(item)
    }

    private fun checkReadExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.READ_EXTERNAL_STORAGE)) {
                requestReadExtStorage()
                if (_statusReadExternalStorage == 0)
                    Snackbar.make(mMainView!!, R.string.explanation_for_request_ex_storage_permission, Snackbar.LENGTH_LONG)
                            .setAction(R.string.grant) { requestReadExtStorage() }.show()
            } else
                showNoStoragePermissionSnackBar()
        } else
            _statusReadExternalStorage = 1
    }

    private fun checkWriteExternalStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                requestWriteExtStorage()
                if (_statusWriteExternalStorage == 0)
                    Snackbar.make(mMainView!!, R.string.explanation_for_request_ex_storage_permission, Snackbar.LENGTH_LONG)
                            .setAction(R.string.grant) { requestWriteExtStorage() }.show()
            } else
                showNoStoragePermissionSnackBar()
        } else
            _statusWriteExternalStorage = 1
    }

    private fun requestReadExtStorage() {
        ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                _statusReadExternalStorage)
    }

    private fun requestWriteExtStorage() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE), _statusWriteExternalStorage)
    }

    private fun showNoStoragePermissionSnackBar() {
        Snackbar.make(mMainView!!, R.string.no_ex_storage_permission, Snackbar.LENGTH_LONG)
                .setAction(R.string.settings) {
                    openApplicationSettings()

                    Toast.makeText(applicationContext,
                            R.string.prompt_how_to_grant_ex_storage_permission,
                            Toast.LENGTH_SHORT)
                            .show()
                }
                .show()
    }

    private fun openApplicationSettings() {
        val appSettingsIntent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:$packageName"))
        startActivityForResult(appSettingsIntent, -2)
    }

    private fun setStatus(s: Status) {
        when (s) {
            Status.NEW -> {
                mStatusTextView!!.setText(R.string.status_request_new)
                mStatusView!!.setCardBackgroundColor(ContextCompat.getColor(this, R.color.request_new))
            }
            Status.COMPLETED -> {
                mStatusTextView!!.setText(R.string.status_request_completed)
                mStatusView!!.setCardBackgroundColor(ContextCompat.getColor(this, R.color.request_ready))
            }
            Status.REJECTED -> {
                mStatusTextView!!.setText(R.string.status_request_rejected)
                mStatusView!!.setCardBackgroundColor(ContextCompat.getColor(this, R.color.request_error))
            }
            Status.IN_PROGRESS -> {
                mStatusTextView!!.setText(R.string.status_request_in_progress)
                mStatusView!!.setCardBackgroundColor(ContextCompat.getColor(this, R.color.request_warning))
            }
            else -> {
                mStatusTextView!!.setText(R.string.undefined)
                mStatusView!!.setCardBackgroundColor(ContextCompat.getColor(this, R.color.request_error))
            }
        }
    }

    private fun updateFields(body: ReadRes) {
        mIsViewGroupRequired!!.clear()
        mListViewGroupToFieldId!!.clear()
        mListViewGroupToAttachmentId!!.clear()

        val status = Handler.instance.parseStringToRequestStatus(body.data!!.status!!)
        mStatus = status
        setStatus(status)
        mSwipeRefreshLayout!!.isRefreshing = false
        val form = body.data.form
        mToolbarView!!.title = form!!.title
        mTitleView!!.text = form.title
        if (form.description == null || form.description == "")
            mDescriptionView!!.visibility = View.GONE
        else {
            mDescriptionView!!.visibility = View.VISIBLE
            mDescriptionView!!.text = form.description
        }
        val answerList = body.data.answers
        for (i in 0 until form.fields!!.size) {
            val id = form.fields[i].id
            val answerSubList = ArrayList<ReadRes.Data.Answer>()
            for (answerId in answerList!!.indices)
                if (answerList[answerId].fieldId == id)
                    answerSubList.add(answerList[answerId])
            addField(form.fields[i], answerSubList)
        }

        if (form.fields.isNotEmpty())
            showFieldsLayout(true)
    }

    private fun addField(field: ReadRes.Data.Form.Field, userAnswers: List<ReadRes.Data.Answer>) {
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1f, resources.displayMetrics).toInt()

        addFieldTitle(field, px)
        addFieldDescription(field, px)

        when (Handler.instance.parseFieldType(field.type!!)) {
            TypeField.TEXT -> {
                addTextField(field, userAnswers, px)
                return
            }
            TypeField.SELECT -> {
                if (field.meta!!.multiple!!)
                    addCheckboxField(field, userAnswers, px)
                else
                    addRadioGroupField(field, userAnswers, px)
                return
            }
            TypeField.ATTACHMENT -> addAttachmentField(field, userAnswers)
            else -> {
            }
        }
    }

    private fun addFieldTitle(field: ReadRes.Data.Form.Field, px: Int) {
        val textViewTitle = TextView(this)
        textViewTitle.textSize = 20f
        textViewTitle.typeface = Typeface.DEFAULT_BOLD
        var title = field.title!!
        textViewTitle.text = title
        // one way to add * in the end
        // if android version >= 7.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && field.required!!) {
            title += "<font color=#ee3333>*</font>"
            textViewTitle.text = Html.fromHtml(title, 1)
        }
        // if android version is less than 7.0
        else if (field.required!!) {
            title += "*"
            textViewTitle.text = title
        }
        val titlell = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        titlell.setMargins(0, 0, 0, 8 * px)
        mFieldsLayout!!.addView(textViewTitle, titlell)
    }

    private fun addFieldDescription(field: ReadRes.Data.Form.Field, px: Int) {
        if (field.description == null || field.description == "")
            return

        val textViewDescription = TextView(this)
        textViewDescription.text = field.description
        textViewDescription.textSize = 14f
        val descriptionll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        descriptionll.setMargins(8 * px, 0, 0, 4 * px)
        mFieldsLayout!!.addView(textViewDescription, descriptionll)
    }

    private fun addTextField(field: ReadRes.Data.Form.Field, userAnswers: List<ReadRes.Data.Answer>, px: Int) {
        val editTextValue = EditText(this)
        if (userAnswers.isNotEmpty())
            editTextValue.setText(userAnswers[0].value)
        editTextValue.isEnabled = false
        val valuell = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        valuell.setMargins(0, 0, 0, 12 * px)
        mFieldsLayout!!.addView(editTextValue, valuell)
        val listView = ArrayList<View>()
        listView.add(editTextValue)

        mIsViewGroupRequired!![listView] = field.required!!
        mListViewGroupToFieldId!![listView] = field.id!!

        if (mStatus === Status.NEW)
            addUnlockAndSaveButton(listView)
    }

    private fun addCheckboxField(field: ReadRes.Data.Form.Field, userAnswers: List<ReadRes.Data.Answer>, px: Int) {
        val linearLayoutGroup = LinearLayout(this)
        linearLayoutGroup.orientation = LinearLayout.VERTICAL

        val groupll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        groupll.setMargins(0, 0, 0, 12 * px)

        val checkboxll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        checkboxll.setMargins(0, 0, 0, 4 * px)

        val listView = ArrayList<View>()
        for (i in 0 until field.meta!!.options!!.size) {
            val checkbox = CheckBox(linearLayoutGroup.context)
            val value = field.meta.options!![i]
            checkbox.text = value
            for (j in userAnswers.indices)
                if (userAnswers[j].value == value) {
                    checkbox.isChecked = true
                    break
                }
            checkbox.isEnabled = false
            linearLayoutGroup.addView(checkbox, checkboxll)
            listView.add(checkbox)
        }
        mFieldsLayout!!.addView(linearLayoutGroup, groupll)

        mIsViewGroupRequired!![listView] = field.required!!
        mListViewGroupToFieldId!![listView] = field.id!!

        if (mStatus === Status.NEW)
            addUnlockAndSaveButton(listView)
    }

    private fun addRadioGroupField(field: ReadRes.Data.Form.Field, userAnswers: List<ReadRes.Data.Answer>, px: Int) {
        val radioGroup = RadioGroup(this)
        radioGroup.orientation = LinearLayout.VERTICAL
        val radioGroupll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioGroupll.setMargins(0, 0, 0, 12 * px)

        val radioButtonll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        radioButtonll.setMargins(0, 0, 0, 4 * px)

        val listView = ArrayList<View>()
        for (i in 0 until field.meta!!.options!!.size) {
            val radioButton = RadioButton(radioGroup.context)
            val value = field.meta.options!![i]
            radioButton.text = value
            for (j in userAnswers.indices)
                if (userAnswers[j].value == value) {
                    radioButton.isChecked = true
                    break
                }
            radioButton.isEnabled = false
            radioGroup.addView(radioButton, radioButtonll)
            listView.add(radioButton)
        }
        mFieldsLayout!!.addView(radioGroup, radioGroupll)

        mIsViewGroupRequired!![listView] = field.required!!
        mListViewGroupToFieldId!![listView] = field.id!!

        if (mStatus === Status.NEW)
            addUnlockAndSaveButton(listView)
    }

    private fun addAttachmentField(field: ReadRes.Data.Form.Field, userAnswers: List<ReadRes.Data.Answer>) {
        val download = LinearLayout(this)
        download.orientation = LinearLayout.HORIZONTAL
        val button = Button(this)
        button.setText(R.string.download_file)
        download.addView(button)

        val pathForUser = TextView(this)
        pathForUser.setText(R.string.nothing)
        download.addView(pathForUser)

        var tid = 0

        if (userAnswers.isNotEmpty()) {
            pathForUser.text = userAnswers[0].attachment!!.name
            tid = userAnswers[0].attachment!!.id!!
        } else
            button.setText(R.string.choose_file)

        val id = tid

        val listView = ArrayList<View>()
        listView.add(pathForUser)

        val downloadll = LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        button.setOnClickListener { v ->
            if ((v as Button).text.toString() == getString(R.string.download_file))
                downloadFileAsync(id)
            else if (v.text.toString() == getString(R.string.choose_file)) {
                checkReadExternalStoragePermission()
                if (_statusReadExternalStorage == 1) {
                    mmIdField = mListViewGroupToFieldId!![listView]!!
                    showFileChooser(1)
                }
            }
        }
        mFieldsLayout!!.addView(download, downloadll)

        mIsViewGroupRequired!![listView] = field.required!!
        mListViewGroupToFieldId!![listView] = field.id!!
        if (userAnswers.isNotEmpty())
            mListViewGroupToAttachmentId!![listView] = userAnswers[0].attachment!!.id!!

        if (mStatus === Status.NEW && userAnswers.isNotEmpty()) {
            addEditAttachmentButton(listView)
            if (!field.required)
                addDeleteAttachmentButton(listView)
        }
    }

    private fun showFileChooser(request: Int) {
        val intent = Intent()
        intent.type = "*/*"
        val mimetypes = arrayOf("image/jpeg", "image/pjpeg", "image/png", "application/pdf", "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes)
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Choose file to Upload.."), request)
    }

    private fun addUnlockAndSaveButton(views: List<View>) {
        val unlockButton = Button(this)
        unlockButton.setText(R.string.edit)

        unlockButton.setOnClickListener(View.OnClickListener { v ->
            val isEdit = (v as Button).text === getString(R.string.edit)

            if (isEdit && mEditActiveButton != null) {
                showMessageSnackBar(getString(R.string.error_save_before))
                return@OnClickListener
            }
            mEditActiveButton = if (isEdit) v else null

            if (!isEdit) {
                showProgress(true)
                val isRequired = mIsViewGroupRequired!![views]!!

                var answer: String? = null
                val answers = ArrayList<String>()
                when {
                    views[0] is EditText -> answer = (views[0] as EditText).text.toString()
                    views[0] is CheckBox -> for (i in views.indices)
                        if ((views[i] as CheckBox).isChecked)
                            answers.add((views[i] as CheckBox).text.toString())
                    views[0] is RadioButton -> for (i in views.indices)
                        if ((views[i] as RadioButton).isChecked)
                            answers.add((views[i] as RadioButton).text.toString())
                }

                if (isRequired && (answer == null || answer == "") && answers.isEmpty()) {
                    showMessageSnackBar(getString(R.string.fill_fields))
                    showProgress(false)
                    showFieldsLayout(true)
                    return@OnClickListener
                }

                val idField = mListViewGroupToFieldId!![views]!!
                editFieldAsync(idField, answer ?: answers, false)
            }

            for (i in views.indices)
                views[i].isEnabled = isEdit
            unlockButton.setText(if (isEdit) R.string.save else R.string.edit)
        })
        mFieldsLayout!!.addView(unlockButton)
    }

    private fun addEditAttachmentButton(views: List<View>) {
        val buttonEdit = Button(this)
        buttonEdit.setText(R.string.upload_file)
        buttonEdit.setOnClickListener {
            mmIdAttachment = mListViewGroupToAttachmentId!![views]!!
            mmPathForUser = views[0] as TextView
            checkReadExternalStoragePermission()
            if (_statusReadExternalStorage == 1)
                showFileChooser(0)
        }
        mFieldsLayout!!.addView(buttonEdit)
    }

    private fun addDeleteAttachmentButton(views: List<View>) {
        val buttonDelete = Button(this)
        buttonDelete.setText(R.string.delete_file)
        buttonDelete.setOnClickListener {
            deleteFileAsync(mListViewGroupToAttachmentId!![views]!!)
        }
        mFieldsLayout!!.addView(buttonDelete)
    }

    private fun setNewPathForUser(fileName: String) {
        mmPathForUser!!.text = fileName
    }

    private fun checkSelectedFile(uri: Uri): Boolean {
        val pathToFile = PathUtil.getPath(this@ReadSubmissionActivity, uri)
        if (pathToFile == null || pathToFile == "") {
            showMessageSnackBar(getString(R.string.file_not_found))
            return false
        }
        val file = File(pathToFile)
        if (file.length() >= 5 * 1024 * 1024) {
            showMessageSnackBar(getString(R.string.limit_size))
            return false
        }
        val pattern = Pattern.compile("\\.(jpg|jpeg|png|doc|docx|xls|xlsx|pdf)$")
        if (!pattern.matcher(file.name).find()) {
            showMessageSnackBar(getString(R.string.allow_file_format))
            return false
        }
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // 0 == edit file
        // 1 == upload file
        if (resultCode == RESULT_OK && (requestCode == 0 || requestCode == 1)) {
            if (data == null)
                return

            val selectedFileUri = data.data!!
            if (checkSelectedFile(selectedFileUri)) {
                if (requestCode == 0)
                    editFileAsync(selectedFileUri)
                else
                    uploadFileAsync(selectedFileUri)
            }
        }
        // request read ex storage permission
        else if (requestCode == -1) {
            _statusReadExternalStorage = 1
        }
        // request write ex storage permission
        else if (requestCode == -2) {
            _statusWriteExternalStorage = 1
        }
    }

    private fun showProgress(isVisible: Boolean) {
        mProgressView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible)
            mFieldsLayout!!.visibility = View.GONE
    }

    private fun showFieldsLayout(isVisible: Boolean) {
        mFieldsLayout!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    override fun onRefresh() {
        mFieldsLayout!!.removeAllViews()
        getSubmissionAsync()
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainView!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
