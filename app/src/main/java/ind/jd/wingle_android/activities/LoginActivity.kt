package ind.jd.wingle_android.activities

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.CountDownTimer
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.auth.code.CodeReq
import ind.jd.wingle_android.server.responses.auth.code.CodeRes
import ind.jd.wingle_android.server.responses.auth.email.EmailReq
import ind.jd.wingle_android.server.responses.auth.resend.ResendReq
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException
import java.util.*
import java.util.regex.Pattern


class LoginActivity : AppCompatActivity() {

    private var mCountDownTimer: CountDownTimer? = null

    private var mCallStack: Stack<View>? = null

    private var mProgressView: View? = null
    private var mMainView: View? = null

    private var mEmailForm: View? = null
    private var mEmailView: EditText? = null

    private var mSignInForm: View? = null
    private var mSignInTimer: TextView? = null
    private var mSignInResentCodeButton: TextView? = null
    private var mSignInCodeView: EditText? = null

    private var mSh: SharedPreferences? = null
    private val mPauseSeconds: Long = 60

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        if (savedInstanceState == null)
            initializeFields()
    }

    private fun initializeFields() {
        mCallStack = Stack()
        mSh = getSharedPreferences("Data", MODE_PRIVATE)

        mProgressView = findViewById(R.id.LOGIN_progress)
        mMainView = findViewById(R.id.LOGIN)

        mEmailForm = findViewById(R.id.LOGIN_EMAIL_form)
        mEmailView = findViewById(R.id.LOGIN_EMAIL_email)
        val mNextStepButton = findViewById<CardView>(R.id.LOGIN_EMAIL_next_step_button)
        mNextStepButton.setOnClickListener {
            clearTimer()
            requestSendEmailAsync()
        }

        mSignInForm = findViewById(R.id.LOGIN_CODE_form)
        mSignInCodeView = findViewById(R.id.LOGIN_CODE_code)
        val mSignInButton = findViewById<CardView>(R.id.LOGIN_CODE_login_button)
        mSignInButton.setOnClickListener { requestSignInAsync() }
        mSignInResentCodeButton = findViewById(R.id.LOGIN_CODE_resend_code_button)
        mSignInResentCodeButton!!.setOnClickListener { requestResendCodeAsync() }
        mSignInTimer = findViewById(R.id.LOGIN_CODE_timer)

        mCallStack!!.push(mEmailForm)

        mSh!!.edit().clear().apply()
    }

    override fun onBackPressed() {
        clearTimer()
        if (mProgressView!!.visibility == View.VISIBLE)
            return
        if (mCallStack!!.size == 1) {
            super.onBackPressed()
            return
        }
        mCallStack!!.pop().visibility = View.GONE
        closeKeyboard()
        mCallStack!!.peek().visibility = View.VISIBLE
        mSignInCodeView!!.setText("")
    }

    private fun clearTimer() {
        if (mCountDownTimer != null) {
            mCountDownTimer!!.cancel()
            mCountDownTimer = null
        }
    }

    private fun requestSendEmailAsync() {
        // reset errors
        mEmailView!!.error = null

        val email = mEmailView!!.text.toString()

        var cancel = false
        var focusView: View? = null
        val pattern = Pattern.compile("^.+@edu\\.hse\\.ru$")

        // check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView!!.error = getString(R.string.error_field_required)
            focusView = mEmailView
            cancel = true
        } else if (!pattern.matcher(email).find()) {
            mEmailView!!.error = getString(R.string.error_invalid_email)
            focusView = mEmailView
            cancel = true
        }

        // show found errors
        if (cancel) {
            focusView!!.requestFocus()
        } // run email request on server
        else {
            showProgress(true)

            NetworkService.instance
                    .jsonApi
                    .email(EmailReq(email))
                    .enqueue(object : Callback<ResponseBody> {
                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                            showProgress(false)
                            // errors in field
                            if (response.code() == 422) {
                                showEmailForm(true)
                                showMessageSnackBar(getString(R.string.field_filling_error))
                            }
                            // success
                            else if (response.code() == 204) {
                                showSignInForm(true)
                                if (BuildConfig.DEBUG)
                                    Log.i("JaDill", "OK in SendEmailTask")
                            } // unexpected error
                            else {
                                showEmailForm(true)
                                showMessageSnackBar(getString(R.string.error_server))
                            }
                        }

                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            showProgress(false)
                            showEmailForm(true)
                            showMessageSnackBar(getString(
                                    if (t is UnknownHostException)
                                        R.string.no_internet
                                    else
                                        R.string.error_server)
                            )

                            if (BuildConfig.DEBUG)
                                Log.e("JaDill", "Throw exception in SendEmailTask\n\n" + t.message)
                        }
                    })
        }
    }

    private fun requestSignInAsync() {
        // reset error
        mSignInCodeView!!.error = null

        val email = mEmailView!!.text.toString()
        val code = mSignInCodeView!!.text.toString()
        val intCode: Int

        var cancel = false
        var focusView: View? = null

        try {
            intCode = Integer.parseInt(code)
        } catch (ex: NumberFormatException) {
            mSignInCodeView!!.error = getString(R.string.error_only_digits)
            mSignInCodeView!!.requestFocus()
            return
        }

        if (TextUtils.isEmpty(code)) {
            mSignInCodeView!!.error = getString(R.string.error_field_required)
            focusView = mSignInCodeView
            cancel = true
        }

        // show found errors
        if (cancel) {
            focusView!!.requestFocus()
        } // run code request on server
        else {
            showProgress(true)

            NetworkService.instance.jsonApi
                    .code(CodeReq(email, intCode))
                    .enqueue(object : Callback<CodeRes> {
                        override fun onResponse(call: Call<CodeRes>, response: Response<CodeRes>) {
                            showProgress(false)
                            // errors in field
                            if (response.code() == 422) {
                                showEmailForm(true)
                                showMessageSnackBar(getString(R.string.field_filling_error))
                            } // too many requests
                            else if (response.code() == 429) {
                                showEmailForm(true)
                                showMessageSnackBar(getString(R.string.too_many_requests))
                            } // success
                            else if (response.code() == 200 && response.body() != null) {
                                val codeRes = response.body()!!.data
                                val et = mSh!!.edit()
                                et.putString(NameFieldSharedPreferences.instance.token(), codeRes!!.token)
                                et.putString(NameFieldSharedPreferences.instance.fio(), codeRes.user!!.fio)
                                et.putString(NameFieldSharedPreferences.instance.email(), codeRes.user.id)
                                et.putInt(NameFieldSharedPreferences.instance.defaultCourse(), codeRes.settings!!.defaultCourse!!)
                                et.apply()
                                startActivity(Intent(this@LoginActivity, MainActivity::class.java))
                                finish()
                                if (BuildConfig.DEBUG)
                                    Log.i("JaDill", "OK in SendCodeTask")
                            } // unexpected error
                            else {
                                showEmailForm(true)
                                showMessageSnackBar(getString(R.string.error_server))
                            }
                        }

                        override fun onFailure(call: Call<CodeRes>, t: Throwable) {
                            showProgress(false)
                            showSignInForm(true)
                            showMessageSnackBar(getString(
                                    if (t is UnknownHostException)
                                        R.string.no_internet
                                    else
                                        R.string.error_server)
                            )

                            if (BuildConfig.DEBUG)
                                Log.e("JaDill", "Throw exception in SendCodeTask\n\n" + t.message)
                        }
                    })
        }
    }

    private fun requestResendCodeAsync() {
        val email = mEmailView!!.text.toString()

        NetworkService.instance.jsonApi
                .resendCode(ResendReq(email))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        showProgress(false)
                        // success
                        if (response.code() == 204) {
                            showAboutResendCode()
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in ResendCodeTask")
                        } // unexpected error
                        else {
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in ResendCodeTask\n\n" + t.message)
                    }
                })
    }

    private fun showProgress(isVisible: Boolean) {
        mProgressView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            closeKeyboard()
            mEmailForm!!.visibility = View.GONE
            mSignInForm!!.visibility = View.GONE
        }
    }

    private fun closeKeyboard() {
        val focusView = this.currentFocus
        if (focusView != null) {
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(focusView.windowToken, 0)
        }
    }

    private fun showEmailForm(isVisible: Boolean) {
        mEmailForm!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        mCallStack!!.clear()
        mCallStack!!.push(mEmailForm)
    }

    private fun showSignInForm(isVisible: Boolean) {
        mSignInCodeView!!.setText("")
        mSignInForm!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (mCallStack!!.peek() !== mSignInForm)
            mCallStack!!.push(mSignInForm)
        if (mCountDownTimer == null) {
            mSignInResentCodeButton!!.visibility = View.GONE
            mSignInTimer!!.visibility = View.VISIBLE
            val messageFirst = getString(R.string.resend_code_prompt) + " " + mPauseSeconds + " " + getString(R.string.seconds_)
            mSignInTimer!!.text = messageFirst
            mCountDownTimer = object : CountDownTimer(mPauseSeconds * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val time = Math.round(millisUntilFinished / 1000.0)
                    var messageTick = getString(R.string.resend_code_prompt) + " " + time + " "
                    if (time == 1L)
                        messageTick += getString(R.string.second)
                    else when (time % 10) {
                        0L, in 5L..9L -> messageTick += getString(R.string.seconds_)
                        in 2L..4L -> messageTick += getString(R.string.seconds_i)
                        1L -> messageTick += getString(R.string.seconds_u)
                    }
                    mSignInTimer!!.text = messageTick
                }

                override fun onFinish() {
                    mCountDownTimer = null
                    mSignInResentCodeButton!!.visibility = View.VISIBLE
                    mSignInTimer!!.visibility = View.GONE
                }
            }
            mCountDownTimer!!.start()
        }
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainView!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun showAboutResendCode() {
        showMessageSnackBar(getString(R.string.we_resend_code))
        if (mCountDownTimer == null) {
            mSignInResentCodeButton!!.visibility = View.GONE
            mSignInTimer!!.visibility = View.VISIBLE
            mCountDownTimer = object : CountDownTimer(mPauseSeconds * 1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    val time = Math.round(millisUntilFinished / 1000.0)
                    var messageTick = getString(R.string.resend_code_prompt) + " " + time + " "
                    if (time == 1L)
                        messageTick += getString(R.string.second)
                    else when (time % 10) {
                        0L, in 5L..9L -> messageTick += getString(R.string.seconds_)
                        in 2L..4L -> messageTick += getString(R.string.seconds_i)
                        1L -> messageTick += getString(R.string.seconds_u)
                    }
                    mSignInTimer!!.text = messageTick
                }

                override fun onFinish() {
                    mCountDownTimer = null
                    mSignInResentCodeButton!!.visibility = View.VISIBLE
                    mSignInTimer!!.visibility = View.GONE
                }
            }
            mCountDownTimer!!.start()
        }
    }
}

