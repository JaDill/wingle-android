package ind.jd.wingle_android.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.TextView
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.data.Handler
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.course.get.GetRes
import ind.jd.wingle_android.server.responses.user.me.MeRes
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException


class ProfileActivity : AppCompatActivity() {

    private var mMainView: View? = null
    private var mScrollView: View? = null
    private var mProgress: View? = null
    private var mNameUser: TextView? = null
    private var mEmailUser: TextView? = null
    private var mTypeUser: TextView? = null
    private var mUserInfo: TextView? = null
    private var mStudentNumber: TextView? = null
    private var mDefaultCourse: TextView? = null
    private var mSh: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_HSE_Style)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        initializeFields()
    }

    override fun onResume(){
        super.onResume()
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!

        NetworkService.instance
                .jsonApi
                .getPersonalInfo(token)
                .enqueue(object : Callback<MeRes> {
                    override fun onResponse(call: Call<MeRes>, response: Response<MeRes>) {
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            updateFields(response.body()!!)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetPersonalInfoTask")
                        } // unexpected error
                        else {
                            showProgress(false)
                            showMessageSnackBar(getString(R.string.error_server))
                        }
                    }

                    override fun onFailure(call: Call<MeRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetPersonalInfoTask\n\n" + t.message)
                    }
                })
    }

    private fun initializeFields() {
        val toolbar = findViewById<Toolbar>(R.id.PROFILE_toolbar)
        toolbar.setTitle(R.string.personal_info)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        mMainView = findViewById(R.id.PROFILE)
        mScrollView = findViewById(R.id.PROFILE_info_scroll_view)
        mProgress = findViewById(R.id.PROFILE_progress)

        mSh = getSharedPreferences("Data", MODE_PRIVATE)
        val fio = mSh!!.getString(NameFieldSharedPreferences.instance.fio(), "")
        mNameUser = findViewById(R.id.PROFILE_HEADER_name_user)
        mNameUser!!.text = fio
        val email = mSh!!.getString(NameFieldSharedPreferences.instance.email(), "")
        mEmailUser = findViewById(R.id.PROFILE_HEADER_email_user)
        mEmailUser!!.text = email
        mTypeUser = findViewById(R.id.PROFILE_type)
        mUserInfo = findViewById(R.id.PROFILE_info)
        mStudentNumber = findViewById(R.id.PROFILE_student_number)
        mDefaultCourse = findViewById(R.id.PROFILE_default_course)
        val changeDefaultCourseButton = findViewById<CardView>(R.id.PROFILE_change_course_button)
        changeDefaultCourseButton.setOnClickListener {
            startActivityForResult(Intent(this@ProfileActivity, PickDefaultCourseActivity::class.java), 1)
        }
    }

    private fun updateDefaultCourseAsync(idCourse: Int) {
        showProgress(true)
        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")
        NetworkService.instance.jsonApi
                .getCourseWithID(token!!, idCourse)
                .enqueue(object : Callback<GetRes> {
                    override fun onResponse(call: Call<GetRes>, response: Response<GetRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            setDefaultCourseName(response.body()!!.data!!.title)
                            showScrollView(true)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in UpdateDefaultCourseTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<GetRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in UpdateDefaultCourseTask\n\n" + t.message)
                    }
                })

    }

    private fun setDefaultCourseName(name: String?) {
        mDefaultCourse!!.text = name
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return super.onSupportNavigateUp()
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainView!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun showProgress(isVisible: Boolean) {
        mProgress!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible)
            mScrollView!!.visibility = View.GONE
    }

    private fun showScrollView(isVisible: Boolean) {
        mScrollView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun updateFields(body: MeRes) {
        val data = body.data
        mNameUser!!.text = data!!.user!!.fio
        mEmailUser!!.text = data.user!!.email
        mTypeUser!!.setText(Handler.instance.parseUserRoleFromServer(data.user.type))
        mUserInfo!!.text = data.user.info
        mStudentNumber!!.text = data.user.studentNumber
        updateDefaultCourseAsync(data.settings!!.defaultCourse!!)

        val edit = mSh!!.edit()
        edit.putString(NameFieldSharedPreferences.instance.fio(), data.user.fio)
        edit.putString(NameFieldSharedPreferences.instance.email(), data.user.email)
        edit.putInt(NameFieldSharedPreferences.instance.defaultCourse(), data.settings.defaultCourse!!)
        edit.apply()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // 1 = get new default course
        if (requestCode == 1 && resultCode == RESULT_OK) {
            val id = data!!.getIntExtra(NameFieldSharedPreferences.instance.id(), -1)
            if (id != -1)
                updateDefaultCourseAsync(id)
            else
                showMessageSnackBar(getString(data.getIntExtra(NameFieldSharedPreferences.instance.error(), R.string.error_server)))
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
}

