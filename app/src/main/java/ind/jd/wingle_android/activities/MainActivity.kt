package ind.jd.wingle_android.activities

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.design.widget.NavigationView
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import ind.jd.wingle_android.R
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.fragments.AboutFragment
import ind.jd.wingle_android.fragments.HistoryFragment

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {

    private var mFAB: FloatingActionButton? = null
    private var mDrawerLayout: DrawerLayout? = null
    private var mNavigationView: NavigationView? = null
    private var mToolbar: Toolbar? = null
    private var mSh: SharedPreferences? = null
    var mListFilterStatus: ArrayList<String>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_HSE_Style_Transparent)
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        val sh1 = getSharedPreferences("DataFirstStart", MODE_PRIVATE)
        val isFirstStart = sh1.getBoolean(NameFieldSharedPreferences.instance.firstStart(), true)
        mSh = getSharedPreferences("Data", MODE_PRIVATE)
        // show onBoarding
        if (isFirstStart) {
            val e = sh1.edit()
            e.putBoolean(NameFieldSharedPreferences.instance.firstStart(), false)
            e.apply()
            startOnBoarding()
        } // check auth
        else {
            // проверка на существование авторизации
            val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")
            if (token!!.isEmpty()) {
                startLoginActivity()
            }
        }

        initializeFields()
    }

    private fun initializeFields() {
        mListFilterStatus = resources.getStringArray(R.array.submission_status).toCollection(ArrayList())
        mToolbar = findViewById(R.id.MAIN_toolbar)
        setSupportActionBar(mToolbar)


        mFAB = findViewById(R.id.MAIN_fab)
        mFAB!!.setOnClickListener { startActivity(Intent(this@MainActivity, PickFormActivity::class.java)) }

        mNavigationView = findViewById(R.id.MAIN_nav_view)
        mNavigationView!!.setNavigationItemSelectedListener(this)

        val profile = mNavigationView!!.getHeaderView(0)
        profile.setOnClickListener {
            val drawer = findViewById<DrawerLayout>(R.id.MAIN_drawer_layout)
            drawer.closeDrawer(GravityCompat.START)
            startActivity(Intent(this@MainActivity, ProfileActivity::class.java))
        }

        updatePersonalInfo(profile)

        mDrawerLayout = findViewById(R.id.MAIN_drawer_layout)

        val toggle = ActionBarDrawerToggle(
                this, mDrawerLayout, mToolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        mDrawerLayout!!.addDrawerListener(toggle)
        toggle.syncState()

        supportFragmentManager.beginTransaction().replace(R.id.MAIN_fragment_container,
                createHistoryFragmentWithFilterStatusList()).commit()
        mNavigationView!!.setCheckedItem(R.id.NAV_MENU_history)
    }

    private fun updatePersonalInfo(profile: View) {
        val fio = mSh!!.getString(NameFieldSharedPreferences.instance.fio(), "")
        val email = mSh!!.getString(NameFieldSharedPreferences.instance.email(), "")
        val fullNameField = profile.findViewById<TextView>(R.id.NAV_HEADER_name_user)
        fullNameField.text = fio
        val emailField = profile.findViewById<TextView>(R.id.NAV_HEADER_email_user)
        emailField.text = email
    }

    private fun startLoginActivity() {
        startActivity(Intent(this@MainActivity, LoginActivity::class.java))
        finish()
    }

    private fun startOnBoarding() {
        startActivity(Intent(this@MainActivity, IntroActivity::class.java))
        finish()
    }

    override fun onResume() {
        super.onResume()
        if (mNavigationView!!.checkedItem!!.itemId == R.id.NAV_MENU_history)
            mFAB!!.show()
        else
            mFAB!!.hide()
    }

    override fun onBackPressed() {
        // close left nav menu
        if (mDrawerLayout!!.isDrawerOpen(GravityCompat.START))
            mDrawerLayout!!.closeDrawer(GravityCompat.START)
        else
            super.onBackPressed()
    }

    private fun createHistoryFragmentWithFilterStatusList(): HistoryFragment {
        val args = Bundle()
        args.putStringArrayList(NameFieldSharedPreferences.instance.filter(), mListFilterStatus)
        val historyFragment = HistoryFragment()
        historyFragment.arguments = args
        return historyFragment
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.NAV_MENU_history -> {
                supportFragmentManager.beginTransaction().replace(R.id.MAIN_fragment_container,
                        createHistoryFragmentWithFilterStatusList()).commit()
                mFAB!!.show()
                mToolbar!!.menu!!.findItem(R.id.HISTORY_MENU_filter).isVisible = true
            }
            R.id.NAV_MENU_help -> startOnBoarding()
            R.id.NAV_MENU_review -> {
                val intent = Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", "dillinger.jacob78@gmail.com", null))
                startActivity(Intent.createChooser(intent, "Type your review"))
            }
            R.id.NAV_MENU_about -> {
                supportFragmentManager.beginTransaction().replace(R.id.MAIN_fragment_container, AboutFragment()).commit()
                mFAB!!.hide()
                mToolbar!!.menu!!.findItem(R.id.HISTORY_MENU_filter).isVisible = false
            }
            R.id.NAV_MENU_logout -> {
                startActivity(Intent(this@MainActivity, LoginActivity::class.java))
                finish()
            }
            else -> Snackbar.make(mFAB!!.rootView, R.string.action_is_not_exist, Snackbar.LENGTH_LONG).show()
        }
        mDrawerLayout!!.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.history_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.HISTORY_MENU_filter) {
            val listFilterStatus: ArrayList<String>? = mListFilterStatus
            val checkedArray = resources.getStringArray(R.array.submission_status).map { listFilterStatus!!.contains(it) }.toBooleanArray()
            val dialogBuilder = AlertDialog.Builder(this@MainActivity)
            dialogBuilder.setTitle(R.string.select_status_filter)
                    .setMultiChoiceItems(R.array.submission_status, checkedArray) { _, which, isChecked ->
                        if (isChecked)
                            listFilterStatus!!.add(resources.getStringArray(R.array.submission_status)[which])
                        else if (listFilterStatus!!.contains(resources.getStringArray(R.array.submission_status)[which]))
                            listFilterStatus.remove(resources.getStringArray(R.array.submission_status)[which])
                    }
                    .setPositiveButton(R.string.ok) { dialog, _ ->
                        mListFilterStatus = listFilterStatus
                        dialog.dismiss()
                        supportFragmentManager.beginTransaction().replace(R.id.MAIN_fragment_container,
                                createHistoryFragmentWithFilterStatusList()).commit()
                    }
                    .setNegativeButton(R.string.cancel) { dialog, _ ->
                        dialog.cancel()
                    }
                    .create().show()
        } else
            Snackbar.make(mNavigationView!!, R.string.action_is_not_exist, Snackbar.LENGTH_LONG).show()
        return super.onOptionsItemSelected(item)
    }
}
