package ind.jd.wingle_android.activities

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.adapters.CourseAdapter
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.server.NetworkService
import ind.jd.wingle_android.server.responses.course.index.IndexRes
import ind.jd.wingle_android.server.responses.user.settings.SettingsReq
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException

class PickDefaultCourseActivity : AppCompatActivity() {

    private var mMainView: View? = null

    private var mToolbar: Toolbar? = null
    private var mProgressView: View? = null
    private var mNoElementsView: View? = null
    private var mCoursesList: RecyclerView? = null

    private var mSh: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_default_course)

        initializeFields()
        getCoursesAsync()
    }

    private fun initializeFields() {
        mMainView = findViewById(R.id.PICK_DEFAULT_COURSE_FORM)
        mSh = getSharedPreferences("Data", MODE_PRIVATE)

        mToolbar = findViewById(R.id.PICK_DEFAULT_COURSE_FORM_toolbar)
        mToolbar!!.setTitle(R.string.pick_course)
        setSupportActionBar(mToolbar)
        try {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        mProgressView = findViewById(R.id.PICK_DEFAULT_COURSE_FORM_progress)
        mNoElementsView = findViewById(R.id.PICK_DEFAULT_COURSE_FORM_no_elements)

        val linearLayoutManagerForCourseRecyclerView = LinearLayoutManager(this)

        mCoursesList = findViewById(R.id.PICK_DEFAULT_COURSE_FORM_COURSE_recycler_view)
        mCoursesList!!.layoutManager = linearLayoutManagerForCourseRecyclerView
    }

    private fun showProgress(isVisible: Boolean) {
        mProgressView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            mCoursesList!!.visibility = View.GONE
            mNoElementsView!!.visibility = View.GONE
        }
    }

    private fun showNoElements(isVisible: Boolean) {
        mNoElementsView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun showCourses(isVisible: Boolean) {
        mCoursesList!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun getCoursesAsync() {
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance.jsonApi
                .getCourses(token)
                .enqueue(object : Callback<IndexRes> {
                    override fun onResponse(call: Call<IndexRes>, response: Response<IndexRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            if (response.body()!!.data != null && response.body()!!.data!!.isNotEmpty()) {
                                val listAdapter = CourseAdapter(this@PickDefaultCourseActivity, response.body()!!.data!!, ::setNewDefaultCourseAsync)
                                mCoursesList!!.adapter = listAdapter
                                showCourses(true)
                            } else
                                showNoElements(true)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetCoursesTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<IndexRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetCoursesTask\n\n" + t.message)
                    }
                })
    }

    private fun setNewDefaultCourseAsync(idCourse: Int) {
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance.jsonApi
                .setNewDefaultCourse(token, SettingsReq(idCourse))
                .enqueue(object : Callback<ResponseBody> {
                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200) {
                            val edit = mSh!!.edit()
                            edit.putInt(NameFieldSharedPreferences.instance.defaultCourse(), idCourse)
                            edit.apply()
                            intent.putExtra(NameFieldSharedPreferences.instance.id(), idCourse)
                            setResult(RESULT_OK, intent)
                            finish()
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in SetNewDefaultCourseTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in SetNewDefaultCourseTask\n\n" + t.message)
                    }

                })
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainView!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}