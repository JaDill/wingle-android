package ind.jd.wingle_android.activities

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.View
import ind.jd.wingle_android.BuildConfig
import ind.jd.wingle_android.R
import ind.jd.wingle_android.adapters.CategoryAdapter
import ind.jd.wingle_android.adapters.FormAdapter
import ind.jd.wingle_android.data.NameFieldSharedPreferences
import ind.jd.wingle_android.server.NetworkService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.UnknownHostException
import java.util.*

class PickFormActivity : AppCompatActivity() {

    private var mMainView: View? = null
    private var mSh: SharedPreferences? = null
    private var mCallStack: Stack<View>? = null

    private var mToolbar: Toolbar? = null
    private var mProgressView: View? = null
    private var mNoElementsView: View? = null
    private var mCategoriesList: RecyclerView? = null
    private var mFormsList: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pick_form)

        initializeFields()
        val idDefaultCourse = mSh!!.getInt(NameFieldSharedPreferences.instance.defaultCourse(), 0)
        getCategoriesAsync(idDefaultCourse)
    }

    private fun initializeFields() {
        mMainView = findViewById(R.id.PICK_FORM)
        mSh = getSharedPreferences("Data", MODE_PRIVATE)
        mCallStack = Stack()

        mToolbar = findViewById(R.id.PICK_FORM_toolbar)
        mToolbar!!.setTitle(R.string.pick_course)
        setSupportActionBar(mToolbar)
        try {
            supportActionBar!!.setDisplayHomeAsUpEnabled(true)
            supportActionBar!!.setDisplayShowHomeEnabled(true)
        } catch (e: NullPointerException) {
            e.printStackTrace()
        }

        mProgressView = findViewById(R.id.PICK_FORM_progress)
        mNoElementsView = findViewById(R.id.PICK_FORM_no_elements)

        val linearLayoutManagerForCategoryRecyclerView = LinearLayoutManager(this)

        mCategoriesList = findViewById(R.id.PICK_FORM_CATEGORY_recycler_view)
        mCategoriesList!!.layoutManager = linearLayoutManagerForCategoryRecyclerView

        val linearLayoutManagerForFormRecyclerView = LinearLayoutManager(this)

        mFormsList = findViewById(R.id.PICK_FORM_FORM_recycler_view)
        mFormsList!!.layoutManager = linearLayoutManagerForFormRecyclerView
    }

    private fun showProgress(isVisible: Boolean) {
        mProgressView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
        if (isVisible) {
            mCategoriesList!!.visibility = View.GONE
            mFormsList!!.visibility = View.GONE
            mNoElementsView!!.visibility = View.GONE
        }
    }

    private fun showNoElements(isVisible: Boolean) {
        mNoElementsView!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun showCategories(isVisible: Boolean) {
        mCategoriesList!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun showForms(isVisible: Boolean) {
        mFormsList!!.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun getCategoriesAsync(idCourse: Int) {
        mToolbar!!.setTitle(R.string.pick_category)
        mCallStack!!.push(mCategoriesList)
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance
                .jsonApi
                .getAllCourseCategories(token, idCourse)
                .enqueue(object : Callback<ind.jd.wingle_android.server.responses.category.index.IndexRes> {
                    override fun onResponse(call: Call<ind.jd.wingle_android.server.responses.category.index.IndexRes>, response: Response<ind.jd.wingle_android.server.responses.category.index.IndexRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            if (response.body()!!.data != null && response.body()!!.data!!.isNotEmpty()) {
                                val listAdapter = CategoryAdapter(this@PickFormActivity, response.body()!!.data!!, ::getFormsAsync)
                                mCategoriesList!!.adapter = listAdapter
                                showCategories(true)
                            } else
                                showNoElements(true)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetCategoriesTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ind.jd.wingle_android.server.responses.category.index.IndexRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetCategoriesTask\n\n" + t.message)
                    }
                })
    }

    private fun getFormsAsync(idCategory: Int) {
        mToolbar!!.setTitle(R.string.pick_form)
        mCallStack!!.push(mFormsList)
        showProgress(true)

        val token = mSh!!.getString(NameFieldSharedPreferences.instance.token(), "")!!
        NetworkService.instance
                .jsonApi
                .getAllCategoryForms(token, categoryId = idCategory)
                .enqueue(object : Callback<ind.jd.wingle_android.server.responses.form.index.IndexRes> {
                    override fun onResponse(call: Call<ind.jd.wingle_android.server.responses.form.index.IndexRes>, response: Response<ind.jd.wingle_android.server.responses.form.index.IndexRes>) {
                        showProgress(false)
                        // unauthorized
                        if (response.code() == 401)
                            startLoginActivity()
                        // success
                        else if (response.code() == 200 && response.body() != null) {
                            if (response.body()!!.data != null && response.body()!!.data!!.isNotEmpty()) {
                                val listAdapter = FormAdapter(this@PickFormActivity, response.body()!!.data!!, ::openForm)
                                mFormsList!!.adapter = listAdapter
                                showForms(true)
                            } else
                                showNoElements(true)
                            if (BuildConfig.DEBUG)
                                Log.i("JaDill", "OK in GetCategoriesTask")
                        } // unexpected error
                        else
                            showMessageSnackBar(getString(R.string.error_server))
                    }

                    override fun onFailure(call: Call<ind.jd.wingle_android.server.responses.form.index.IndexRes>, t: Throwable) {
                        showProgress(false)
                        showMessageSnackBar(getString(
                                if (t is UnknownHostException)
                                    R.string.no_internet
                                else
                                    R.string.error_server)
                        )

                        if (BuildConfig.DEBUG)
                            Log.e("JaDill", "Throw exception in GetCategoriesTask\n\n" + t.message)
                    }
                })
    }

    private fun openForm(idForm: Int) {
        val intent = Intent(this@PickFormActivity, CreateSubmissionActivity::class.java)
        intent.putExtra(NameFieldSharedPreferences.instance.id(), idForm)
        startActivityForResult(intent, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        // user sent new request
        if (resultCode == Activity.RESULT_OK && requestCode == 0)
            finish()
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onSupportNavigateUp(): Boolean {
        if (mCallStack!!.size == 1) {
            super.onBackPressed()
            return true
        }
        mCallStack!!.pop().visibility = View.GONE
        showNoElements(false)
        mCallStack!!.peek().visibility = View.VISIBLE
        if (mCallStack!!.peek() === mCategoriesList)
            mToolbar!!.setTitle(R.string.pick_category)
        else
            mToolbar!!.setTitle(R.string.pick_course)
        return false
    }

    override fun onBackPressed() {
        if (mCallStack!!.size == 1) {
            super.onBackPressed()
            return
        }
        mCallStack!!.pop().visibility = View.GONE
        showNoElements(false)
        mCallStack!!.peek().visibility = View.VISIBLE
        if (mCallStack!!.peek() === mCategoriesList)
            mToolbar!!.setTitle(R.string.pick_category)
        else
            mToolbar!!.setTitle(R.string.pick_course)
    }

    private fun showMessageSnackBar(message: String) {
        Snackbar.make(mMainView!!, message, Snackbar.LENGTH_LONG).show()
    }

    private fun startLoginActivity() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }
}
