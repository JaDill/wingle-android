package ind.jd.wingle_android.server.responses.submission.readWithId

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*

class ReadRes {
    @SerializedName("data")
    @Expose
    val data: Data? = null

    inner class Data {
        @SerializedName("id")
        @Expose
        val id: Int? = null
        @SerializedName("status")
        @Expose
        val status: String? = null
        @SerializedName("updated_at")
        @Expose
        private val updated_at: String? = null
        val updatedAt: Date
            get() {
                val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                format.timeZone = TimeZone.getTimeZone("UTC")
                return format.parse(updated_at)
            }
        @SerializedName("form")
        @Expose
        val form: Form? = null
        @SerializedName("answers")
        @Expose
        val answers: List<Answer>? = null
        @SerializedName("replies")
        @Expose
        val replies: List<Reply>? = null

        inner class Form {
            @SerializedName("id")
            @Expose
            val id: Int? = null
            @SerializedName("title")
            @Expose
            val title: String? = null
            @SerializedName("description")
            @Expose
            val description: String? = null
            @SerializedName("fields")
            @Expose
            val fields: List<Field>? = null

            inner class Field{
                @SerializedName("id")
                @Expose
                val id: Int? = null
                @SerializedName("title")
                @Expose
                val title: String? = null
                @SerializedName("description")
                @Expose
                val description: String? = null
                @SerializedName("type")
                @Expose
                val type: String? = null
                @SerializedName("required")
                @Expose
                val required: Boolean? = null
                @SerializedName("meta")
                @Expose
                val meta: Meta? = null

                inner class Meta{
                    @SerializedName("options")
                    @Expose
                    val options: List<String>? = null
                    @SerializedName("multiple")
                    @Expose
                    val multiple: Boolean? = null
                }
            }
        }

        inner class Answer{
            @SerializedName("id")
            @Expose
            val id: Int? = null
            @SerializedName("field_id")
            @Expose
            val fieldId: Int? = null
            @SerializedName("value")
            @Expose
            val value: String? = null
            @SerializedName("attachment")
            @Expose
            val attachment: Attachment? = null

            inner class Attachment{
                @SerializedName("id")
                @Expose
                val id: Int? = null
                @SerializedName("name")
                @Expose
                val name: String? = null
            }
        }

        inner class Reply{

        }
    }
}