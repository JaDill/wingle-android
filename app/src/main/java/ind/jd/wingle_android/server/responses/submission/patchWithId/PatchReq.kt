package ind.jd.wingle_android.server.responses.submission.patchWithId

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PatchReq {
    @SerializedName("answers")
    @Expose
    var answers: List<Answer>? = null

    inner class Answer {
        @SerializedName("field_id")
        @Expose
        var fieldId: Int? = null
        @SerializedName("value")
        @Expose
        var value: Any? = null
    }
}