package ind.jd.wingle_android.server.responses.user.settings

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SettingsReq(
        @SerializedName("default_course")
        @Expose
        val defaultCourse: Int? = null
)