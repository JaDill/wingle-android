package ind.jd.wingle_android.server

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NetworkService{
    private val mRetrofit: Retrofit

    val jsonApi: IWingleAPI
        get() = mRetrofit.create(IWingleAPI::class.java)

    val baseUrl: String
        get() = BASE_URL

    init {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
                .addInterceptor(interceptor)

        mRetrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client.build())
                .build()
    }

    companion object {
        private var mInstance: NetworkService? = null
        private const val BASE_URL = "https://wingle-api.krsq.me/api/v2/"

        val instance: NetworkService
            get() {
                if (mInstance == null) {
                    mInstance = NetworkService()
                }
                return mInstance!!
            }
    }
}