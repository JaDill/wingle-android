package ind.jd.wingle_android.server.responses.submission.readAll

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.text.SimpleDateFormat
import java.util.*


class ReadRes {
    @SerializedName("page")
    @Expose
    val page: Int? = null
    @SerializedName("last_page")
    @Expose
    val lastPage: Int? = null
    @SerializedName("page_size")
    @Expose
    val pageSize: Int? = null
    @SerializedName("total_records")
    @Expose
    val totalRecords: Int? = null
    @SerializedName("data")
    @Expose
    val data: List<Data>? = null

    inner class Data {
        @SerializedName("id")
        @Expose
        val id: Int? = null
        @SerializedName("status")
        @Expose
        val status: String? = null
        @SerializedName("updated_at")
        @Expose
        private val updatedAtPrivate: String? = null
        val updatedAt: Date
            get() {
                val format = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
                format.timeZone = TimeZone.getTimeZone("UTC")
                return format.parse(updatedAtPrivate)
            }

        @SerializedName("form")
        @Expose
        val form: Form? = null

        inner class Form {
            @SerializedName("title")
            @Expose
            val title: String? = null
            @SerializedName("description")
            @Expose
            val description: String? = null
        }
    }
}
