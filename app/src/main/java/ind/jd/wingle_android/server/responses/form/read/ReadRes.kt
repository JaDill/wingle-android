package ind.jd.wingle_android.server.responses.form.read

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReadRes {
    @SerializedName("data")
    @Expose
    val data: Data? = null

    inner class Data {
        @SerializedName("id")
        @Expose
        val id: Int? = null
        @SerializedName("title")
        @Expose
        val title: String? = null
        @SerializedName("description")
        @Expose
        val description: String? = null
        @SerializedName("category_id")
        @Expose
        val categoryId: Int? = null
        @SerializedName("fields")
        @Expose
        val fields: List<Field>? = null

        inner class Field {
            @SerializedName("id")
            @Expose
            val id: Int? = null
            @SerializedName("title")
            @Expose
            val title: String? = null
            @SerializedName("description")
            @Expose
            val description: String? = null
            @SerializedName("type")
            @Expose
            val type: String? = null
            @SerializedName("form_id")
            @Expose
            val formId: Int? = null
            @SerializedName("required")
            @Expose
            val required: Boolean? = null
            @SerializedName("personal")
            @Expose
            val personal: Boolean? = null
            @SerializedName("meta")
            @Expose
            val meta: Meta? = null

            inner class Meta {
                @SerializedName("options")
                @Expose
                val options: List<String>? = null
                @SerializedName("multiple")
                @Expose
                val multiple: Boolean? = null
            }
        }
    }
}