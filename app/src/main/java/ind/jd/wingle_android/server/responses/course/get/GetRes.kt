package ind.jd.wingle_android.server.responses.course.get

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetRes {
    @SerializedName("data")
    @Expose
    val data: Data? = null

    inner class Data {
        @SerializedName("id")
        @Expose
        val id: Int = 0
        @SerializedName("title")
        @Expose
        val title: String? = null
    }
}