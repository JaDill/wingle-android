package ind.jd.wingle_android.server

import ind.jd.wingle_android.server.responses.attachment.create.CreateRes
import ind.jd.wingle_android.server.responses.attachment.get.GetRes
import ind.jd.wingle_android.server.responses.auth.code.CodeReq
import ind.jd.wingle_android.server.responses.auth.code.CodeRes
import ind.jd.wingle_android.server.responses.auth.email.EmailReq
import ind.jd.wingle_android.server.responses.auth.resend.ResendReq
import ind.jd.wingle_android.server.responses.submission.create.CreateReq
import ind.jd.wingle_android.server.responses.submission.patchWithId.PatchReq
import ind.jd.wingle_android.server.responses.submission.readAll.ReadReq
import ind.jd.wingle_android.server.responses.user.me.MeRes
import ind.jd.wingle_android.server.responses.user.settings.SettingsReq
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IWingleAPI {
    @Headers("Accept: application/json")
    @POST("auth/email")
    fun email(@Body req: EmailReq): Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("auth/code")
    fun code(@Body req: CodeReq): Call<CodeRes>

    @Headers("Accept: application/json")
    @POST("auth/resend")
    fun resendCode(@Body req: ResendReq): Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET("submission")
    fun getMySubmissions(@Header("Authorization") token: String,
                         @Query("status[]", encoded = true) status: List<String>,
                         @Query("page_size") pageSize: Int = 100)
            : Call<ind.jd.wingle_android.server.responses.submission.readAll.ReadRes>

    @Headers("Accept: application/json")
    @GET("submission/{id}")
    fun getMySubmissionWithID(@Header("Authorization") token: String,
                              @Path("id") id: Int)
            : Call<ind.jd.wingle_android.server.responses.submission.readWithId.ReadRes>

    @Headers("Accept: application/json")
    @GET("course")
    fun getCourses(@Header("Authorization") token: String,
                   @Query("page_size") pageSize: Int = 100,
                   @Query("query") query: String? = null)
            : Call<ind.jd.wingle_android.server.responses.course.index.IndexRes>

    @Headers("Accept: application/json")
    @GET("course/{courseId}")
    fun getCourseWithID(@Header("Authorization") token: String,
                        @Path("courseId") courseId: Int)
            : Call<ind.jd.wingle_android.server.responses.course.get.GetRes>

    @Headers("Accept: application/json")
    @GET("category")
    fun getAllCourseCategories(@Header("Authorization") token: String,
                               @Query("course_id") courseId: Int,
                               @Query("page_size") pageSize: Int = 100,
                               @Query("query") query: String? = null)
            : Call<ind.jd.wingle_android.server.responses.category.index.IndexRes>

    @Headers("Accept: application/json")
    @GET("form")
    fun getAllCategoryForms(@Header("Authorization") token: String,
                            @Query("course_id") courseId: Int? = null,
                            @Query("category_id") categoryId: Int? = null,
                            @Query("page_size") pageSize: Int = 100,
                            @Query("query") query: String? = null)
            : Call<ind.jd.wingle_android.server.responses.form.index.IndexRes>

    @Headers("Accept: application/json")
    @GET("form/{id}")
    fun getFormWithID(@Header("Authorization") token: String,
                      @Path("id") id: Int)
            : Call<ind.jd.wingle_android.server.responses.form.read.ReadRes>

    @Multipart
    @Headers("Accept: application/json")
    @POST("attachment/")
    fun uploadFile(@Header("Authorization") token: String,
                   @Part("name") name: RequestBody,
                   @Part file: MultipartBody.Part)
            : Call<CreateRes>

    @Multipart
    @Headers("Accept: application/json")
    @POST("attachment/{id}")
    fun editFile(@Header("Authorization") token: String,
                 @Path("id") idAttachment: Int,
                 @Part("name") name: RequestBody,
                 @Part file: MultipartBody.Part,
                 @Part("_method") method: RequestBody)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @POST("submission")
    fun createSubmission(@Header("Authorization") token: String,
                         @Body newSubmission: CreateReq)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET("user/me")
    fun getPersonalInfo(@Header("Authorization") token: String): Call<MeRes>

    @Headers("Accept: application/json")
    @POST("user/settings")
    fun setNewDefaultCourse(@Header("Authorization") token: String,
                            @Body body: SettingsReq)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @PATCH("submission/{id}")
    fun editFieldWithID(@Header("Authorization") token: String,
                        @Path("id") id: Int,
                        @Body body: PatchReq)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @DELETE("submission/{id}")
    fun deleteSubmission(@Header("Authorization") token: String,
                         @Path("id") idSubmission: Int)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @DELETE("attachment/{id}")
    fun deleteAttachment(@Header("Authorization") token: String,
                         @Path("id") idAttachment: Int)
            : Call<ResponseBody>

    @Headers("Accept: application/json")
    @GET("attachment/{id}")
    fun getAttachmentInfo(@Header("Authorization") token: String,
                          @Path("id") id: Int)
            : Call<GetRes>
}
