package ind.jd.wingle_android.server.responses.attachment.get

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetRes {
    @field:SerializedName("data")
    @field:Expose
    val data: Data? = null

    inner class Data {
        @field:SerializedName("id")
        @field:Expose
        val id: Int? = null
        @field:SerializedName("name")
        @field:Expose
        val name: String? = null
    }
}