package ind.jd.wingle_android.server.responses.auth.resend

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResendReq(
        @field:SerializedName("email")
        @field:Expose
        private val email: String
)