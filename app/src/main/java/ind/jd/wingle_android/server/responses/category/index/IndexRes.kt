package ind.jd.wingle_android.server.responses.category.index

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class IndexRes {
    @SerializedName("page")
    @Expose
    val page: Int? = null
    @SerializedName("last_page")
    @Expose
    val lastPage: Int? = null
    @SerializedName("page_size")
    @Expose
    val pageSize: Int? = null
    @SerializedName("total_records")
    @Expose
    val totalRecords: Int? = null
    @SerializedName("data")
    @Expose
    val data: List<Data>? = null

    inner class Data {
        @SerializedName("id")
        @Expose
        val id: Int = 0
        @SerializedName("title")
        @Expose
        val title: String? = null
        @SerializedName("description")
        @Expose
        val description: String? = null
        @SerializedName("order")
        @Expose
        val order: Int? = null
        @SerializedName("course_id")
        @Expose
        val courseId: Int? = null
    }
}
