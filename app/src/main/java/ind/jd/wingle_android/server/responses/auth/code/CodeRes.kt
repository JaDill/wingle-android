package ind.jd.wingle_android.server.responses.auth.code

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CodeRes {
    @SerializedName("data")
    @Expose
    val data: Data? = null

    inner class Data {
        @SerializedName("token")
        @Expose
        val token: String? = null
        @SerializedName("user")
        @Expose
        val user: User? = null
        @SerializedName("settings")
        @Expose
        val settings: Settings? = null

        inner class User {
            @SerializedName("id")
            @Expose
            val id: String? = null
            @SerializedName("uns")
            @Expose
            val uns: String? = null
            @SerializedName("externalId")
            @Expose
            val externalId: Int? = null
            @SerializedName("type")
            @Expose
            val type: String? = null
            @SerializedName("fio")
            @Expose
            val fio: String? = null
            @SerializedName("info")
            @Expose
            val info: String? = null
        }

        inner class Settings {
            @SerializedName("default_course")
            @Expose
            val defaultCourse: Int? = null
        }
    }
}

