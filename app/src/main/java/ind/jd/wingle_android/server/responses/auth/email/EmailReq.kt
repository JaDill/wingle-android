package ind.jd.wingle_android.server.responses.auth.email

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EmailReq(@field:SerializedName("email")
               @field:Expose
               private val email: String)
