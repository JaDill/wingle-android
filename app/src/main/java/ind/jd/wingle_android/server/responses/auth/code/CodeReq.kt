package ind.jd.wingle_android.server.responses.auth.code

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class CodeReq(
        @SerializedName("email")
        @Expose
        val email: String,
        @SerializedName("code")
        @Expose
        val code: Int
        )