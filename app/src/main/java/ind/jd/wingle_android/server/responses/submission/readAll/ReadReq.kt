package ind.jd.wingle_android.server.responses.submission.readAll

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ReadReq(
        @SerializedName("status")
        @Expose
        private val statusFilter: List<String>? = null,
        @SerializedName("page_size")
        @Expose
        private val pageSize: Int = 100
)